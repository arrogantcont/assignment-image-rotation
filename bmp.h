#pragma once

#include "common.h"

enum read_status {
	READ_OK = 0,
	READ_INVALID_HEADER,
	READ_INVALID_BITS,
	READ_BAD_FILE
};

enum read_status from_bmp(FILE* in, struct image* img);

enum read_status open_and_read_bmp(const char* path, struct image* img);

enum write_status {
	WRITE_OK = 0,
	WRITE_INVALID_HEADER,
	WRITE_INVALID_BITS,
	WRITE_BAD_FILE
};

enum write_status to_bmp(FILE* out, const struct image* img);

enum write_status open_and_write_bmp(const char* path, const struct image* img);