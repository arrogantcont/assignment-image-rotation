#define _CRT_SECURE_NO_WARNINGS
#include "bmp.h"

struct image rotate(const struct image* img) {
	struct image result = new_image(img->height, img->width);
	uint64_t old_y = 0;
	uint64_t new_x = 0;
	while (old_y < img->height) {
		uint64_t old_x = 0;
		uint64_t new_y = 0;
		while (old_x < img->width) {
			set_pixel(&result, new_x, new_y, get_pixel(img, old_x, old_y));
			old_x++;
			new_y++;
		}
		old_y++;
		new_x++;
	}
	return result;
}

int main() {
	const char* path = "bmp.bmp";
	struct image img;
	const enum read_status read = open_and_read_bmp(path, &img);
	if (read != READ_OK) {
		return read;
	}
	const struct image rotated = rotate(&img);
	const enum write_status write = open_and_write_bmp(path, &rotated);
	delete_image(&img);
	delete_image(&rotated);
	return (int)(write);
}