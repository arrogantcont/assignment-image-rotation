#pragma once

#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>

void* malloc_or_abort(const size_t size);

struct pixel {
	uint8_t b;
	uint8_t g;
	uint8_t r;
};

struct image {
	uint64_t width;
	uint64_t height;
	struct pixel* data;
};

struct image new_image(const uint64_t width, const uint64_t height);

struct pixel get_pixel(const struct image* img, const uint64_t x, const uint64_t y);

void set_pixel(struct image* img, const uint64_t x, const uint64_t y, const struct pixel pxl);

void delete_image(const struct image* img);