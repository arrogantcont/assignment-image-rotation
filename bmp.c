#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include "bmp.h"

static bool fread_success(void* buffer, const size_t size, FILE* file) {
	return fread(buffer, size, 1, file) == 1;
}

static bool fwrite_success(const void* buffer, const size_t size, FILE* file) {
	return fwrite(buffer, size, 1, file) == 1;
}

#pragma pack(push, 1)

struct bmp_header {
	uint16_t file_type;
	uint32_t file_size;
	uint32_t _; // reserved space
	uint32_t pixels_offset;
	uint32_t header_size;
	uint32_t width;
	uint32_t height;
};

#pragma pack(pop)

static bool is_header_valid(const struct bmp_header* header) {
	const uint16_t bmp_file_type = 0x4D42;
	return header->file_type == bmp_file_type;
}

static struct bmp_header read_header(FILE* in) {
	struct bmp_header header;
	if (!fread_success(&header, sizeof(struct bmp_header), in)) {
		return (struct bmp_header){0};
	}
	return header;
}

static struct image read_image(FILE* in, const struct bmp_header* header) {
	struct image img = new_image(header->width, header->height);
	const size_t size_bytes = (size_t)(img.width) * sizeof(struct pixel);
	for (uint64_t y = img.height; y-- != 0; ) {
		if (!fread_success(img.data + img.width * y, size_bytes, in)) {
			delete_image(&img);
			return (struct image){0};
		}
		char alignment[4];
		fread_success(alignment, img.width % 4, in);
	}
	return img;
}

enum read_status from_bmp(FILE* in, struct image* img) {
	const struct bmp_header header = read_header(in);
	if (!is_header_valid(&header)) {
		return READ_INVALID_HEADER;
	}
	if (fseek(in, header.pixels_offset, SEEK_SET) != 0) {
		return READ_INVALID_HEADER;
	}
	*img = read_image(in, &header);
	const bool should_have_pixels = header.width != 0 && header.height != 0;
	if (should_have_pixels && img->data == NULL) {
		return READ_INVALID_BITS;
	}
	return READ_OK;
}

enum read_status open_and_read_bmp(const char* path, struct image* img) {
	FILE* in = fopen(path, "rb");
	if (in == NULL) {
		return READ_BAD_FILE;
	}
	const enum read_status result = from_bmp(in, img);
	fclose(in);
	return result;
}

enum write_status to_bmp(FILE* out, const struct image* img) {
	struct bmp_header header = {0};
	const uint8_t* from  = (const uint8_t*)(&header);
	const uint8_t* until = (const uint8_t*)(&header.width);
	if (!fread_success(&header, until - from, out)) {
		return WRITE_INVALID_HEADER;
	}
	const fpos_t current_position = ftell(out);
	fsetpos(out, &current_position); // required between reads and writes
	const uint32_t width  = (uint32_t)img->width;
	const uint32_t height = (uint32_t)img->height;
	const bool wrote_size =
		fwrite_success(&width,  sizeof(uint32_t), out) &&
		fwrite_success(&height, sizeof(uint32_t), out);
	if (!wrote_size) {
		return WRITE_INVALID_HEADER;
	}
	if (fseek(out, header.pixels_offset, SEEK_SET) != 0) {
		return WRITE_INVALID_HEADER;
	}
	for (uint64_t y = img->height; y-- != 0; ) {
		if (!fwrite_success(img->data + img->width * y, (size_t)(img->width) * sizeof(struct pixel), out)) {
			return WRITE_INVALID_HEADER;
		}
		const struct pixel alignment = {0};
		fwrite_success(&alignment, img->width % 4, out);
	}
	return WRITE_OK;
}

enum write_status open_and_write_bmp(const char* path, const struct image* img) {
	FILE* out = fopen(path, "r+b");
	if (out == NULL) {
		return WRITE_BAD_FILE;
	}
	const enum write_status result = to_bmp(out, img);
	fclose(out);
	return result;
}