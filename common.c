#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include "common.h"

void* malloc_or_abort(const size_t size) {
	void* allocated = malloc(size);
	if (allocated != NULL) {
		return allocated;
	} else {
		abort(); // no memory => can't continue
	}
}

struct image new_image(const uint64_t width, const uint64_t height) {
	const size_t area = (size_t)(width * height);
	struct pixel* data = malloc_or_abort(area * sizeof(struct pixel));
	if (data != NULL) {
		return (struct image){width, height, data};
	} else {
		return (struct image){0, 0, NULL};
	}
}

struct pixel get_pixel(const struct image* img, const uint64_t x, const uint64_t y) {
	return img->data[img->width * y + x];
}

void set_pixel(struct image* img, const uint64_t x, const uint64_t y, const struct pixel pxl) {
	img->data[img->width * y + x] = pxl;
}

void delete_image(const struct image* img) {
	free(img->data);
}